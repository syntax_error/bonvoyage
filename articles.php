<head>
	<meta charset="UTF-8">

</head>
<div class="row featurette">
	<h1 class="page-header">
		<p>Aktualności</p>
	</h1>
</div>

<?php foreach ($news as $aricle): ?>
	<div class="row featurette">
		<div class="col-md-7">
			<h2 class="featurette-heading">
				<span class="text-muted"> <?php echo $aricle['Title'] ?> </span>
			</h2>
			<p class="lead"><?php echo $aricle['Content'] ?></p>
		</div>
		<div class="col-md-5"> 
		 	<img class="featurette-image img-fluid center-block" src=<?php echo $aricle['Photo'] ?> >
		</div>
	</div>
	<hr class="featurette-divider">
<?php endforeach ?>
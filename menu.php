<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
				<span class="sr-only">Menu</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="/">Bon Voyage</a>
		</div>
	 	<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
				
				<li>
				 	<a href=<?php echo "/main_page.php?id="."news"?>>		
				 		Aktualności
				 	</a>
				</li>
				<li>
					<a href="2">O nas</a>
				</li>
				<li class="dropdown hidden-xs">
					<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" aria-expended="false">
						Golden Retriever
						<i class="fa fa-caret-down"></i>
					</a>
					 <ul class="dropdown-menu submenu-dropdown" role="menu">
						<li>
							<a href="#" target="_blank" data-title="wzor">Wzór</a>
						</li>
						<li>
							<a href="#" target="_blank" data-title="NaszeSuki">Nasze suki</a>
						</li>
						<li>
							<a href="#" target="_blank" data-title="Szczeniaki">Szczeniaki</a>
						</li>
						<li>
							<a href="#" target="_blank" data-title="Zdrowie">Zdrowie</a>
						</li>
						<li>
							<a href="#" target="_blank" data-title="Galeria">Galeria</a>
						</li>
					</ul> 
				</li>
				<li>
					<a href="3">Kontakt</a>
				</li>
			</ul> 
		</div>
	</div> 
</nav>